# Slice

Slice is a very simple utility to select columns from the output of a
command.

With it, you can do things like:

```sh
cat myfile.txt | slice 0
```

rather than:

```sh
cat myfile.txt | awk '{print $1}'
```


## Examples

Select several columns:

```sh
cat myfile.txt | slice 2 4 8
```

Select the first and the last field:

```sh
cat myfile.txt | slice 0 -1
```

Select a range:

```sh
cat myfile.txt | slice 3:6
```

Use a different delimiter:

```sh
cat mydata.csv | slice -d, 3:6
```

Change the delimiter from a tab to a comma:

```sh
cat mydata.tsv | slice -d\t -o, :
```

Get help:

```sh
slice -h
```


## Installation

Just copy `slice` to any directory in your PATH. I do it in
`~/.local/bin`.


## "Features"

- Zero-indexed
- Field selection syntax similar to Python's slices
- Negative indexing from end of line
- Optional start/end index


## See also

As many, I found inconvenient to remember how to use `cut` when I had
to from time to time.

Slice was inspired by
[choose](https://github.com/theryangeary/choose), which is probably a
much better tool, but I thought I could write a very simple script
that covered my needs better. That is all.
